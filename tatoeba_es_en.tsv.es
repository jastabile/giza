¡ Intentemos algo !
Tengo que irme a dormir .
¡ Hoy es 18 de junio y es el cumpleaños de Muiriel !
Ahora , Muiriel tiene 20 años .
La contraseña es &quot; Muiriel &quot; .
Volveré pronto .
No tengo palabras .
Esto no acabará nunca .
Simplemente no sé qué decir ...
Era un conejo malo .
Yo estaba en las montañas .
¿ Es una foto reciente ?
No sé si tengo tiempo .
La educación en este mundo me decepciona .
Eso va a costar 30 € .
Gano 100 € al día .
Quizás me dé pronto por vencido y en lugar de eso me eche una siesta .
Eso es porque no quieres estar solo .
Eso no va a pasar .
A veces él puede ser un chico raro .
Me voy a esforzar por no molestarte en tus estudios .
Sólo puedo preguntarme si acaso es lo mismo para todos los demás .
Supongo que es diferente cuando lo consideras a largo plazo .
Los llamaré cuando regrese mañana .
Siempre me gustaron más los personajes misteriosos .
Deberías dormir .
Les dije que me mandaran otro boleto .
Eres tan impaciente conmigo .
No puedo vivir así .
Una vez quise ser astrofísico .
Nunca me gustó la biología .
La última persona a la que le conté mi idea pensó que yo estaba loco .
Si el mundo no fuera como es , yo podría confiar en cualquiera .
Desafortunadamente es verdad .
Están muy ocupados peleándose entre ellos , como para preocuparse por los ideales comunes .
La mayoría de la gente cree que estoy loco .
¡ No , no lo soy ! ¡ Lo eres tú !
¡ Esa es mi frase !
¡ ¡ Él está pateándome ! !
¿ Estás seguro ?
Entonces tenemos un problema ...
¡ Ah , allí hay una mariposa !
¡ Date prisa !
No me sorprende .
Por alguna razón , me siento más vivo por la noche .
Eso depende del contexto .
¡ ¿ Acaso te quieres burlar de mí ? !
Es la cosa más estúpida que he dicho nunca .
¡ ¡ No quiero ser patético , quiero ser guay ! !
Cuando crezca , quiero ser un rey .
América es un lugar encantador para vivir , si estás aquí para ganar dinero .
Estoy tan gordo .
¿ Entonces qué ?
Voy a pegarle un tiro .
No soy un pez de verdad , sólo soy un peluche .
¡ Yo sólo lo digo !
Eso fue probablemente lo que influenció su decisión .
Siempre me he preguntado cómo será tener hermanos .
Esto es lo que yo habría dicho .
Me tomaría una eternidad explicarte todo .
Eso es porque eres una chica .
Algunas veces no puedo evitar mostrar mis sentimientos .
Es una palabra para la que me gustaría encontrar una sustituta .
Eso sería algo que habría que programar .
No tengo la intención de ser egoísta .
Pensemos en lo peor que podría pasar .
¿ Cuántos amigos íntimos tienes ?
Yo puedo ser antisocial , pero eso no significa que no hable con la gente .
Así ha sido siempre .
Creo que lo mejor es no ser maleducado .
Uno siempre puede encontrar tiempo .
Yo seré infeliz , pero no me suicidaría .
Cuando yo estaba en el instituto me levantaba a las 6 todas las mañanas .
Cuando me desperté estaba triste .
Por fin está más o menos explicado .
Yo pensaba que te gustaba aprender cosas nuevas .
La mayoría de la gente escribe sobre su vida cotidiana .
Si pudiera enviarte un malvavisco , Trang , lo haría .
Para hacer eso , tienes que arriesgarte .
Todas las personas que están solas , están solas porque tienen miedo de los demás .
¿ Por qué lo preguntas ?
No soy un artista . Nunca tuve mente para ello .
No puedo decírselo ahora . Ya no es tan simple .
Soy una persona que tiene muchos defectos , pero esos defectos pueden ser corregidos fácilmente .
Cada vez que encuentro algo que me gusta , es demasiado caro .
¿ Cuánto tiempo estuviste ?
Quizás sea lo mismo para él .
La inocencia es una cosa hermosa .
Los humanos nunca fueron hechos para vivir eternamente .
No quiero perder mis ideas , aunque algunas de ellas sean un poco extremas .
