SIGUIENDO
https://masatohagiwara.net/using-giza-to-obtain-word-alignment-between-bilingual-sentences.html

- Bajar sentence es y en
- bajar links
- make en giza++

python preprocessors/tatoeba/create_bitext.py --languages spa_eng --sentences sentences.csv  --links links.csv > tatoeba_es_en.tsv

crea un archivo con formato:
ID \t LANG \t sent \t ID \t LANG \t sent


cut -f3 tatoeba_es_en.tsv | mosesdecoder/scripts/tokenizer/tokenizer.perl -l es \
    > tatoeba_es_en.tsv.es
cut -f6 tatoeba_es_en.tsv | mosesdecoder/scripts/tokenizer/tokenizer.perl -l en \
    > tatoeba_es_en.tsv.en

Crea un archivo por lenguaje con las sentencias tokenizadas (separando token por espacio)


./plain2snt.out tatoeba_es_en.tsv.es tatoeba_es_en.tsv.en

genera un archivo de vocabulario (vcb) y uno de sentencias (snt) por cada lenguaje
El formato del vocabulario es:
ID word freq

El formato del de sentencias es (del .en_…_es.snt):
1
ID_EN ID_EN ID_EN ID_EN ID_EN
ID_ES ID_ES ID_ES ID_ES
1

